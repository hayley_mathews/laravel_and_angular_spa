var commentApp = angular.module('commentApp', ['mainCtrl', 'commentService']);

commentApp.filter('unsafe', function($sce) {
    return function(val) {
	return $sce.trustAsHtml(val);
    };
});
